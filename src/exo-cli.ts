let running = true;
let tab:string[] = [];

/**
 * Boucle infinie qui va exécuter une chose ou une autre selon la valeur qu'on
 * entre dans un prompt
 */
while(running) {
    let command = prompt('Which command to execute ?');

    if(command == 'exit') {
        running = false;
        console.log('Bye');
    }

    if(command == 'add') {
        addToList();
    }
    if(command == 'display') {
        display();
    }
}
/**
 * Fonction qui affiche le contenu du tableau ou un message spécial s'il est vide
 * (on utilise ici le return pour stopper l'exécution de la fonction, on aurait pu faire
 * un else à la place)
 */
function display() {
    if(tab.length < 1) {
        console.log('The array is empty');
        return;
    }
    
    console.log('Array Length : '+tab.length);
    for(let item of tab) {
        console.log(item); 
    }
}

/**
 * Fonction qui ajoute une nouvelle valeur dans le tableau, avec vérification qu'une 
 * valeur a bien été entrée
 */
function addToList() {
    let text  = prompt('Enter a new thing');
    if(text) {
        tab.push(text);
    } 
}