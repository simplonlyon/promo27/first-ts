let age = 26;
let isOfAge = false;

if(age >= 18) {
    isOfAge = true;
} else {
    isOfAge = false;
}


if(isOfAge) {
    console.log('Welcome to the tax paying app');
} else {
    console.log('Go home');
}


let a = 10;
let b = 5;
let operator = prompt('choose an operator');
let result = 0;

if(operator == '+') {
    result = a+b;
} else if(operator == '-') {
    result = a-b;
} else if(operator == '*') {
    result = a*b;
} else if (operator == '/') {
    result = a/b;
} else {
    console.log('invalid operator');
}
//Pour faire la même chose on peut utiliser le switch qui est spécialement fait pour
//ce genre de condition où on exécute une chose ou une autre selon la valeur contenue dans une variable
switch (operator) {
    case '+': //Chaque case va permettre de dire que faire si la variable du switch vaut la valeur en question
        result = a+b;
        break; //et chaque case doit être terminé par un break;
    case '-':
        result = a-b;
        break;
    case '*':
        result = a*b;
        break;
    case '/':
        result = a/b;
        break;
    default: //Le default ne s'exécutera que si la variable ne contient aucune des valeurs des cases
        console.log('invalid operator');
        break;
}

//Ici on fait de la concaténation, c'est à dire insérer la valeur d'une variable dans une chaîne de caractère
//on peut soit concaténé avec des + (c'est la manière historique)
console.log('result for '+a+' '+operator+' '+b+' = '+result);
//Soit depuis ES6, en utilisant les backtick on peut insérer des variable entourées par ${}, ce qui donne  généralement un résultat un peu plus lisible
console.log(`result for ${a} ${operator} ${b} = ${result}`);
