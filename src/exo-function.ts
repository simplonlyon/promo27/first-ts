
doLoop(4, 'pardon');

doLoop(10, 'autre  chose', true);


/**
 * Fonction qui fait des tours de boucle et affiche des trucs
 * @param loops le nombre de tour de boucle à faire
 * @param toDisplay le truc à console log
 * @param oddOnly est-ce qu'on affiche que les tours de boucle impairs
 */
function doLoop(loops: number, toDisplay: string, oddOnly = false): void {
    
    for (let i = 0; i < loops; i++) { 
        if(isOdd(i) || !oddOnly) {
            console.log(i + ' ' + toDisplay);
        }
    }

}

/**
 * Fonction qui vérifie si un nombre est impair
 * @param num le nombre à vérifier s'il est impair
 * @returns true si impair, false si pair
 */
function isOdd(num: number): boolean {

    return num % 2 != 0;

}