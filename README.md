# first-ts

## How To use
1. Cloner le projet
2. Exécuter `npm i` pour installer les dépendances
3. Exécuter `npm start` pour lancer le projet
4. Accéder au projet sur http://localhost:5173

## Structure d'un projet
En général, les projets de développement auront une structure comme on l'a fait pour celui ci avec :
à la racine :
* **package.json** => fichier qui contient toutes les informations du projet (sa version, les scripts de lancement, la liste de ses dépendances/librairies)
* **README.md** => qui contiendra en markdow une présentation du projet
* **.gitignore** => pour indiquer les dossiers/fichiers qu'on souhaite que git ne prenne pas en compte (en général on aura le dossier node_modules, dist, et d'autres)
* **node_module** => dossier contenant tous les fichiers des dépendances du projet qu'on a installé avec npm
* **dist** => dossier contient le résultat du build du projet (c'est généralement le contenu de ce dossier qu'on mettra en ligne au moment du déploiement)
* **src** => dossier contenant les sources, ce que sont les sources va dépendre un peu selon les projets, mais en JS/TS, on aura les fichier typescript
* **public** => dossier contenant les fichiers static, les images, le favicon, potentiellement le css et les graphismes

## Exercices
### Premières conditions ([ts](src/exo-condition.ts))
1. Créer un nouveau fichier exo-condition.html et un fichier exo-condition.ts et les lier ensemble
2. Dans le fichier ts, créer 2 variables, age et isOfAge et faire une condition qui va faire que si age est supérieur ou égal à 18, alors on assigne true à isOfAge
3. En dessous, faire une autre condition qui va vérifier si isOfAge est true, et si oui, on affiche un message avec un console.log genre 'Welcome to the tax paying app' et sinon on affiche 'Go home kiddo'

### Condition calcul ([ts](src/exo-condition.ts))
1. A la suite du fichier exo-condition.ts, créer 2 nouvelles variables a et b qui contiendront n'importe quoi comme nombre (genre 3 et 5)
2. Créer une troisième variable operator qui contiendra une chaîne de caractère
3. Faire plusieurs conditions qui vont vérifier : si operator vaut '+' alors on additionne a et b et on affiche le résultat, si ça contient '-' soustraction, '/' division et '*' multiplication
4. Si on a aucun des opérateurs possibles, on affiche un message 'incorrect operator' avec un console.log
5. à la place de mettre directement une valeur dans operator, faire en sorte de lui assigner un prompt('choose an operator') qui affichera un popup permettant à l'utilisateur·ice d'entrer un opérateur directement 

### Exo Pyramide ([ts](src/exo-pyramid.ts))
Objectif final, avoir ça via des boucles : 
```
    *
   ***
  *****
 *******
*********
```
1. Créer un nouveau point d'entrée exo-pyramid.html / exo-pyramid.ts
2. Pour commencer, on peut essayer de faire une première boucle pour avoir autant de console log qu'on a d'étage à la pyramide, donc ici 5 (peu importe ce qu'on console log)
3. Ensuite, on peut essayer de trouver un calcul très savant pour afficher à chaque tour de boucle le nombre d'étoile par étage de la pyramide, donc genre ça comme résultat :
```
1
3
5
7
9
```
4. Une fois qu'on a le bon nombre d'étage et une variable qui contient le nombre d'étoile à afficher par étage, "ya pu qu'à" faire en sorte d'afficher les étoiles en questions (par exemple avec une boucle imbriquée qui concatène dans une phrase, un repeat ou autre)
Résultat :
```
*
***
*****
*******
*********
```
5. Ensuite on recommence un peu la même chose qu'à l'étape 3, mais cette fois ci pour le nombre d'espace avant chaque étoile pour avoir un truc qui ressemble à ça :
```
4*
3***
2*****
1*******
0*********
```
6. Enfin, on fait comme à l'étape 4 pour concaténer les espaces avant les étoiles, et on obtient notre pyramide
   
Bonus : Pouvoir changer le nombre d'étage avec une variable et pourquoi pas aussi le caractère (pour pouvoir faire une pyramide de coeurs par exemple <3)

### Exo Fonction ([ts](src/exo-function.ts))
1. Dans le projet first-ts, créer un fichier exo-function html/ts
2. Créer une fonction doLoop() qui va faire une boucle for qui console log coucou 10 fois
3. Rajouter un paramètre loops:number qui va indiquer à la boucle combien de tour on fait
4. Rajouter un autre paramètre toDisplay:string qui va indiquer ce qui sera console log à la place de coucou
5. Créer une autre fonction isOdd(num:number):boolean qui va vérifier si le num donné en paramètre est impair, renvoyé true si c'est le cas et false  sinon (vous pouvez faire et tester cette fonction même si l'autre ne marche pas ou n'existe pas, elle est indépendante)
6. Dans la fonction doLoop, dans la boucle, utiliser la fonction isOdd sur l'itérateur (le compteur de la boucle) pour n'afficher que les tours de boucle impair
7. Rajouter un troisième paramètre oddOnly:boolean et modifier la condition pour faire qu'on affiche les impair uniquement si oddOnly est true

### Exo Array ([ts](src/exo-array.ts))
Exo tableau
1. Créer un fichier html / ts exo-array (fichier ts dans src et fichier html à la racine)
2. Dans le typescript, créer une variable promo typée en `string[]`
3. Dans cette variable assigner un tableau avec 3-4 prénoms à l'intérieur en chaîne de caractères, séparés par des virgules
4. En dessous de la variable, faire un push pour rajouter 1 ou 2 autres prénoms
5. Puis utiliser une boucle, soir un for...i (avec compteur, celle qu'on fait depuis le début) soit un for...of pour parcourir le tableau, et pour chaque valeur de celui ci afficher en console le prénom et combien de lettre il y a dans le prénom
6. Déclarer une variable total au dessus la boucle, et à chaque tour de boucle rajouter la length de chaque prénom dedans, après la boucle afficher le total de lettre dans tous les prénoms

### Exo CLI ([ts](src/exo-cli.ts))
1. Créer un fichier html / ts exo-cli (fichier ts dans src et fichier html à la racine)
2. Dans le TS, faire une variable `running` initialisée à true et faire une boucle while(running) 
3. Dans cette boucle, faire une variable `command` à laquelle on va assigner un prompt
4. Faire une condition sur la variable command, et si celle ci contient 'exit', on passe la valeur de running à false, ce qui devrait arrêter la boucle et les prompt
5. Tout en haut du fichier, créer un tableau de string[] et l'initialisé vide puis où vous voulez, créer une fonction addToList() qui va demander du texte avec un prompt et push le résultat dans le tableau
6. Dans la boucle, faire en sorte que si command vaut 'add' alors on déclenche la fonction addToList
7. Faire une fonction display() qui va afficher la taille du tableau puis boucler sur le tableau pour afficher chaque valeur. Rajouter un nouveau if pour si command vaut 'display' et faire en sorte d'y déclencher cette fonction