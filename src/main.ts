//Les variables
let variableName = 'la valeur';
variableName = '100';
console.log(variableName);

let variableBoolean = true;
variableBoolean = false;

let variableNumber = 100;
variableNumber = 10.5;


let ecrireUnePhraseEnCamelCase = 10;

let firstName = 'Jean';
firstName = 'Rama';


//Les incrémentations
let count = 0;
count++;
//pareil que faire
count += 1;
//pareil que faire
count = count+1;
//Mais c'est pas pareil du tout de faire
count+1 //ça, ça fait rien, ou en tout cas, ça change pas la valeur de count, pasqu'il n'y a pas d'assignation

count += 5; //augmente la valeur de count de 5

count--;
count -= 1;

//Les concaténations
let phrase = 'Salut, je suis ';
phrase += 'dév'; // La variable phrase contient maintenant 'Salut, je suis dév'
phrase += variableName; // La variable phrase contient maintenant 'Salut, je suis dév100'


console.log('Je suis une phrase avec une concaténation '+variableName);
console.log('Phrase '+variableName+' avec '+variableName+' plusieurs concaténations');
console.log(`La même qu'au dessus ${variableName} mais ${variableName} avec des
backticks qui permettent d'utiliser cette syntaxe
et de sauter des lignes`);


//Les conditions
if (variableName != 'coucou' && variableNumber > 10) {
  console.log('condition vraie');
} else {
  console.log('condition fausse');
}

//Les boucles
let counter = 0;
//La boucle while attend un booléen en argument et continuera à tourner tant que ce booléen est vrai
//à chaque tour de boucle, on vérifie si le booléen est toujours true.
while(counter < 10) {
  console.log('coucou '+counter);
  counter++;
}

//Autre boucle plus commune pour faire un nombre de tour donné selon un compteur, c'est
//la boucle for qui attendra en premier une déclaration de variable, en deuxième la condition de la boucle 
//et en troisième la modification qui sera appliquée à l'itérateur à chaque tour de boucle 
for(let i = 0; i < 10; i++) {
  console.log(i);
}
//Les fonctions